#!/bin/bash

reference=$1
target=$2
output=$3

[[ -z $output ]] && exit 1

imcp $target $output
fslcpgeom $reference $output
