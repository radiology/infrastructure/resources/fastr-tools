import argparse
from functools import partial
import sys
import os
import subprocess
import shlex
import platform
from fastr import exceptions
import shutil
import tkinter
import tkinter.messagebox


GAMEsBin = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'bin', 'GAMEs')
isPosix = True
if platform.system() == 'Windows':
    GAMEsBin += '.exe'
    isPosix = False


def call_GAMEs(command_line, args):
    print ('Command line {0}'.format(command_line))
    lexed_cmd = shlex.split(command_line, posix=isPosix)
    try:
        process = subprocess.Popen(lexed_cmd, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        (logpipe, errpipe) = process.communicate() #block until finished
        for line in logpipe.splitlines() :
            print line
        for line in errpipe.splitlines() :
            print line
        #tkMessageBox.showinfo('SUCCESS', line)    
        return True
    except OSError as exception:
        #tkMessageBox.showinfo('EXCEPT', str(exception))
        print os.strerror(exception.errno) + " " + command_line + ": " + " ".join(args)
        return False
    except:
        #tkMessageBox.showinfo('EXCEPT', 'Unknown')
        return False


def restricted_float(x, lower=sys.float_info.min, upper=sys.float_info.max):
    x = float(x)
    if x < lower or x > upper:
        raise argparse.ArgumentTypeError('Argument value: {0} not in range [{1}, {2}]'.format(x, lower, upper))
    return x


def readable_file(filepath):
    if (not os.path.isfile(filepath)) or (not os.access(filepath, os.R_OK)):
        raise argparse.ArgumentTypeError('Could no open file {0} for read'.format(filepath))
    return filepath


def writeable_dir(dir):
    if (not os.path.exists(dir)) or (not os.access(dir, os.W_OK)):
        raise argparse.ArgumentTypeError('Could no open dir {0} for write'.format(dir))
    return dir


def readable_dir(dir):
    if (not os.path.exists(dir)) or (not os.access(dir, os.R_OK)):
        raise argparse.ArgumentTypeError('Could no open dir {0} for read'.format(dir))
    return dir


float_between_zero_and_one = partial(restricted_float, lower=0, upper=1)
def add_common_args(parser):
    parser.add_argument('--nPerm', type=int, help='Number of permutation repetitions e.g. 10000')

    parser.add_argument('--type', choices=['unpaired', 'paired'],
                        default='unpaired', help=('Optional default unpaired (test).'
                                                  'If true perform the paired test'
                                                  'If false perform the unpaired test'))



    parser.add_argument('--alpha', type=float_between_zero_and_one,
                        default=0.05,
                        help=('P-value cutoff, '
                              'Only displacements for nodes with p-values'
                              'below this cutoff are recorded'))

    parser.add_argument('--g0', type=int, help='First group to compare ',
                        required=True)

    parser.add_argument('--g1', type=int, help='Second group to compare ',
                        required=True)

    parser.add_argument('--path', type=writeable_dir, help=('A writeable directory where the GAMEs model '
                                                            'file system will be created and where the results '
                                                            'will be written'), required=True)


def main():
    #tkMessageBox.showinfo('INFO', 'Starting GAMEsAnalyzeModel')
    #tkMessageBox.showinfo('INFO', str(sys.argv))
    parser = argparse.ArgumentParser(description='GAMEsAnalyseModel: Perform permutation tests on a GAMEs shape model'
                                                 'produced by GAMEsBuildModel - either single or repeated')

    add_common_args(parser)

    subparsers = parser.add_subparsers(help='Choose simple (SIMP) or repeated (REP) permutation tests',
                                       dest='subcommand')

    parser_simp = subparsers.add_parser('SIMP', help='Simple permutation tests - one repetition')
    parser_simp.add_argument('--pName', default='pvalue.txt', help=('Output file name for the pvalues'
                                                                    'default is pvalue.txt'))

    parser_simp.add_argument('--dName', default='dvalue.txt', help=('Output file name for the displacement values'
                                                                    'filtered by alpha default is dvalue.txt.'
                                                                    'An unfiltered version will be produced'
                                                                    'with _unfiltered in the file name'))

    parser_rep = subparsers.add_parser('REP', help='Repeated permutation tests - nRep repetitions')

    parser_rep.add_argument('--nRep', type=int, default=20, help='Number of repeated permutation tests - default 20')
    parser_rep.add_argument('--frac', type=float_between_zero_and_one, default=0.5, help='Fraction of population to '
                                                                                         'use in each rep, default'
                                                                                         '0.5')
    parser_rep.add_argument('--mName', default='mvalue_rep.txt', help=('Output file name for the median values '
                                                                       '- default mvalue_rep.txt'))
    parser_rep.add_argument('--cName', default='cvalue_rep.txt', help=('Output file name for the classes '
                                                                       '0,0-0.3,>0.3-0.6,>0.6-1 values '
                                                                       '- default cvalue_rep.txt'))
    parser_rep.add_argument('--fName', default='fvalue_rep.txt', help=('Output file name for the labels file - default fvalue_rep.txt'))

    parser_rep.add_argument('--dName', default='dvalue_rep.txt', help=('Output file name for the displacement values'
                                                                       'filtered by alpha default is dvalue.txt.'
                                                                       'An unfiltered version will be produced'
                                                                       'with _unfiltered in the file name'))
    args = None    
    try:
        #tkMessageBox.showinfo('INFO', 'About to parse arguments ')
        args = parser.parse_args()
    except argparse.ArgumentError as e:
        #tkMessageBox.showinfo('EXCEPT', 'Parsing arguments ')
        #tkMessageBox.showinfo('INFO', str(e))
        exit(1)

    #tkMessageBox.showinfo('INFO', 'Arg parse succeeded')
    # if that went well then call the model making functions one by one
    # 1) Create the directory CREATE_FS
    command_line = ''
    if args.subcommand == 'SIMP':
        command_line = (GAMEsBin + ' -mod SIMP_PERM -path ' + args.path +
                        ' -a ' + str(args.alpha) + ' -nPerm ' + str(args.nPerm) +
                        ' -type ' + args.type + ' -g0 ' + str(args.g0) +
                        ' -g1 ' + str(args.g1) + ' -pName ' + args.pName +
                        ' -dName ' + args.dName + ' -type ' + args.type)
    elif args.subcommand == 'REP':
        command_line = (GAMEsBin + ' -mod REP_PERM -path ' + args.path +
                        ' -a ' + str(args.alpha) + ' -nPerm ' + str(args.nPerm) +
                        ' -type ' + args.type + ' -g0 ' + str(args.g0) +
                        ' -g1 ' + str(args.g1) +
                        ' -dName ' + args.dName +
                        ' -cName ' + args.cName +
                        ' -mName ' + args.mName +
                        ' -fName ' + args.fName +
                        ' -nRep ' + str(args.nRep) + ' -perc ' + str(args.frac))
        #tkMessageBox.showinfo('INFO', 'Calling: ' + command_line)

    call_GAMEs(command_line, args)

if __name__ == "__main__":
    main()
