import argparse
from functools import partial
import sys
import os
import subprocess
import shlex
import platform
from fastr import exceptions
import tkinter
import tkinter.messagebox
import shutil


GAMEsBin = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'bin', 'GAMEs')
#print 'GAMEs binary at', GAMEsBin, ' ', __file__
isPosix = True
if platform.system() == 'Windows':
    GAMEsBin += '.exe'
    isPosix = False


def call_GAMEs(command_line, args):
    print ('Command line {0}'.format(command_line))
    #tkMessageBox.showinfo('INFO', 'Command line: ' + command_line)
    lexed_cmd = shlex.split(command_line, posix=isPosix)
    try:
        process = subprocess.Popen(lexed_cmd, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        (logpipe, errpipe) = process.communicate() #block until finished
        for line in logpipe.splitlines() :
            print line
        for line in errpipe.splitlines() :
            print line
        return True
    except OSError as exception:
        print os.strerror(exception.errno) + " " + command_line + ": " + " ".join(args)
        return False
    except:
        return False


def restricted_float(x, lower=sys.float_info.min, upper=sys.float_info.max):
    x = float(x)
    if x < lower or x > upper:
        raise argparse.ArgumentTypeError('Argument value: {0} not in range [{1}, {2}]'.format(x, lower, upper))
    return x


def readable_file(filepath):
    if (not os.path.isfile(filepath)) or (not os.access(filepath, os.R_OK)):
        raise argparse.ArgumentTypeError('Could no open file {0} for read'.format(filepath))
    return filepath

def writeable_dir(dir):
    if (not os.path.exists(dir)) or (not os.access(dir, os.W_OK)):
        raise argparse.ArgumentTypeError('Could no open dir {0} for write'.format(dir))
    return dir


def readable_dir(dir):
    if (not os.path.exists(dir)) or (not os.access(dir, os.R_OK)):
        raise argparse.ArgumentTypeError('Could no open dir {0} for read'.format(dir))
    return dir


def main():
    #tkMessageBox.showinfo('INFO', 'Starting GAMEsBuildModel')
    #tkMessageBox.showinfo('INFO', str(sys.argv))
    parser = argparse.ArgumentParser(description='GAMEsBuildModel: Build a GAMEs model from a set input subjects')

    parser.add_argument ('--inpath', type=readable_dir, help=('Optional base path will be prepended '
                                                              'to the all subjects in the group list if present') )
    parser.add_argument ('--subjects', nargs='+', type=str, help='List of subject niftii files')
    parser.add_argument('--groups', nargs='+', type=int, help=('List of group numbers corresponding '
                                                               'to the subject files '
                                                               'e.g. --subjects A B C X Y Z --groups 0 0 0 1 1 1 '
                                                               'Group 0 must be the control group'))
    parser.add_argument('--op', choices=['AND', 'EQUAL', 'LARGER_THAN'], help=('Operand for picking label '
                                                                               'in segmentation file'), required=True)
    parser.add_argument('--val', type=int, help=('Selection value for picking label in selection file.'
                                                 'Used together with op.'
                                                 'A numerical integer value (e.g., 128)'), required=True)
    parser.add_argument('--perc', type=int, choices=list(range(0, 100)), help=('Threshold percentage between 0 and 100'
                                                                        'percentage (0-100) of subjects which have to '
                                                                        'have a voxel set to 1, for the average volume '
                                                                        'to have the same voxel set to 1.'
                                                                        'A common value is 20 (percent)'),
                        required=True)
    float_greq_zero = partial(restricted_float, lower=0)
    parser.add_argument('--acc', type=float_greq_zero, help=('Activity threshold (mm), lower values ensure more '
                                                             'points in the model. A value close to the voxel '
                                                             'dimensions is reasonable. This value is actually'
                                                             ' -ln(a_T) where a_T is the parameter '
                                                             'described in 2.7 of the Ferrarini thesis'), required=True)

    parser.add_argument('--macc', type=float_greq_zero, help=('The average surface accuracy for the model, '
                                                              'a convergence threshold. In acc_T in 2.7 '
                                                              'of the Ferrarini thesis'), required=True)

    parser.add_argument('--path', type=writeable_dir, help=('A writeable directory where the GAMEs model '
                                                            'file system will be created and where the results '
                                                            'will be written'), required=True)

    parser.add_argument('--comAlign', action='store_true', help=('Optional default false. If false do not adjust '
                                                                 'the volume positions based on center of  mass'))
    args = None    
    try:
        #tkMessageBox.showinfo('INFO', 'About to parse arguments ')
        args = parser.parse_args()
    except argparse.ArgumentError as e:
        #tkMessageBox.showinfo('EXCEPT', 'Parsing arguments ')
        #tkMessageBox.showinfo('INFO', str(e))
        exit(1)
        
    #tkMessageBox.showinfo('INFO', 'GAMEsBuildModel - arguments parsed')
    # if that went well then call the model making functions one by one
    # 1) Create the directory CREATE_FS

    command_line = GAMEsBin + ' -mod CREATE_FS -path ' + args.path
    #tkMessageBox.showinfo('INFO', command_line)
    call_GAMEs(command_line, args)


    # 3) copy the inputs to the working directory /Original
    prepend = ''
    if args.inpath:
        prepend = args.inpath

    if len(args.subjects) != len(args.groups):
        raise argparse.ArgumentTypeError('Number of subjects and groups must be equal')
    subj_groups = list(zip(args.subjects, args.groups))
    subj0 = ''
    groups = {}
    uniqueGroups = []
    # copy the subjects to the Original directory and prepare the group command line switches
    for (subj, group) in subj_groups:
        if group in groups:
            groups[group] += "," + subj
        else:
            groups[group] = " -group " + subj
            uniqueGroups.append(group)
        shutil.copy(os.path.join(prepend, subj), os.path.join(os.path.normpath(args.path), 'Original'))
        if len(subj0) == 0:
            subj0 = subj


    # 4) Create the selection file that contains the group information CREATE_SELFILE
    command_line = GAMEsBin + ' -mod CREATE_SELFILE -path ' + args.path
    for group in uniqueGroups:
        command_line += groups[group]
    call_GAMEs(command_line, args)

    # 5) Create average volume aka AVGSP

    command_line = (GAMEsBin + ' -mod AVGSP -path ' + args.path +
                    ' -op ' + args.op + ' -thr ' + str(args.val) +
                    ' -perc ' + str(args.perc) + ' -subj0 ' + subj0)
    call_GAMEs(command_line, args)

    # 6) Extract surface points per subject SPt2
    command_line = (GAMEsBin + ' -mod SPts -path ' + args.path +
                    ' -op ' + args.op + ' -thr ' + str(args.val) +
                    ' -cma ' + str(1 if args.comAlign else 0) +
                    ' -subj0 ' + subj0)
    call_GAMEs(command_line, args)

    # 7) Create averrage mesh AVGMESH
    command_line = (GAMEsBin + ' -mod AVGMESH -path ' + args.path +
                    ' -acc ' + str(args.acc) + ' -macc ' + str(args.macc))


    call_GAMEs(command_line, args)

    # 8) Create point distribution model PDM
    command_line = (GAMEsBin + ' -mod PDM -path ' + args.path +
                    ' -acc ' + str(args.acc) + ' -macc ' + str(args.macc))


    call_GAMEs(command_line, args)

if __name__ == "__main__":
    main()
