#!/bin/bash

function usage {
    echo "Usage: ${BASH_SOURCE[0]} --input <input_image> --dimension 2D/3D --kernel_size 7 --output <output_image>"
    echo "Error: $1"
    exit 1
}

module load fsl/5.0.9

#default values
output_type="float"

while [[ $# -gt 1 ]] ; do
    key="$1"

    case $key in
        --input)
            input_image="$2"
            shift # past argument
        ;;
        --lower_threshold)
            lower_threshold="$2"
            shift # past argument
        ;;
        --upper_threshold)
            upper_threshold="$2"
            shift
        ;;
        --output_type)
            output_type=$2
            shift
        ;;
        --output)
            output_image="$2"
            shift
        ;;
        *)
            # unknown option
        ;;
    esac
    shift # past argument or value
done

[[ -z $input_image ]] && usage "No input image provided"
[[ -z $lower_threshold ]] && usage "No lower_treshold provided"
[[ -z $upper_threshold ]] && usage "No upper_threshold provided"
[[ -z $output_image ]] && usage "No output image location provided"

fslmaths $input_image -thr ${lower_threshold}  -uthr ${upper_threshold} ${output_image} -odt $output_type
