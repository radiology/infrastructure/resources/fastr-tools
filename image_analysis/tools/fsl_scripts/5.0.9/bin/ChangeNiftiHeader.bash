#!/bin/bash

usage(){
  echo "ChangeNiftiHeader <input> <header key> <value> <output>"
  echo "ERROR $1"
  exit 1 
}

echo "input: $1"                                                                                     
echo "header: $2"
echo "value: $3"
echo "output: $4"

[[ -z $4 ]] && usage "Not enough parameters provided"

input_file=$1
header=$2
value=$3
output_file=$4

#Check if input file is readable
[ ! -r $1 ] && usage "The asl input file cannot be read"
[ -z $FSLDIR ] && usage "FSL is not setup properly"
#Create temp folder
tmp_file=$(mktemp -p $PWD)
[ ! -w $tmp_folder ] && usage "Could not create temp file"
#sed_string="s/${header}/${header} = '\''${value}'\''/g"
fslhd -x ${input_file} | sed "s/${header}.*/${header} = '${value}'/g" > ${tmp_file}
imcp ${input_file} ${output_file}
fslcreatehd ${tmp_file} ${output_file}

#delete tmp file
rm ${tmp_file}
