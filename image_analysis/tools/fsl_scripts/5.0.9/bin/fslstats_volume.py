#!/usr/bin/env python
"""
Helper tool for collecting serialized fslstats voxel_volume output from the whole sample into a single txt-file
Usage: fslstats_volume.py filename vox_vol_volumes

Created on Tue May 2 12:33:30 2017

@author: marzwi
"""

import argparse

def main():
    # Parse input arguments
    parser = argparse.ArgumentParser(description='Collects fslstats volume output from the whole sample into a single txt-file')
    parser.add_argument('outfile', type=str, help='Name of the output txt-file with volumes of the sample')
    parser.add_argument('volumes', type=float, nargs='*', metavar=('voxels', 'mm3'), help='Sample serialized vox_vol output from fslstats')
    args = parser.parse_args()

    # Write every second float (= volume in mm^3) to disk
    with open(args.outfile, 'w') as fid:
        for volume in args.volumes[1::2]:
            fid.write('{:f}\n'.format(volume))


if __name__ == "__main__":
    main()
